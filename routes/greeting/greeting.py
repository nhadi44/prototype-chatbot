from flask import Blueprint, jsonify

greeting_bp = Blueprint('greeting', __name__)


@greeting_bp.route('/')
def index():
    return jsonify({'message': 'Hello from greeting api'})