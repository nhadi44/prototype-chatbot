from flask import Flask, jsonify
from config.config import Config
from models.models import db
from flask_migrate import Migrate
import json

from routes.greeting.greeting import greeting_bp
from routes.chat_bot.chat_bot_routes import chat_bot_bp
from routes.training_data.training_data_routes import training_data_bp
from models.models import DataTraining

app = Flask(__name__)
app.config.from_object(Config)

db.init_app(app)
migrate = Migrate(app, db)

with app.app_context():
    data_training = DataTraining.query.all()
    training_data = [{
        'tag': data.tag,
        'patterns': data.patterns,
        'responses': data.responses
    } for data in data_training]
    db.create_all()

    # Menyimpan data training ke file json
    with open('training_data.json', 'w') as outfile:
        json.dump(training_data, outfile)

    # Membaca data training dari file json
    with open('training_data.json') as json_data:
        training_data = json.load(json_data)

app.register_blueprint(greeting_bp, url_prefix='/greeting')
app.register_blueprint(chat_bot_bp, url_prefix='/chat-bot')
app.register_blueprint(training_data_bp, url_prefix='/training-data')

if __name__ == '__main__':
    app.run(debug=True)
