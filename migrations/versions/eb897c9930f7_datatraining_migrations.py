"""DataTraining migrations

Revision ID: eb897c9930f7
Revises: 
Create Date: 2023-12-21 11:37:09.633645

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'eb897c9930f7'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        'data_training',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('tag', sa.String(length=100), nullable=False),
        sa.Column('patterns', sa.PickleType, nullable=False),
        sa.Column('responses', sa.PickleType, nullable=False),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade() -> None:
    op.drop_table('data_training')
