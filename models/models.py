from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class DataTraining(db.Model):
    __tablename__ = 'data_training'

    id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.String(100), nullable=False)
    patterns = db.Column(db.PickleType, nullable=False)
    responses = db.Column(db.PickleType, nullable=False)

    def __repr__(self):
        return '<DataTraining %r>' % self.id
