from flask import Blueprint, jsonify, request
from models.models import DataTraining, db

training_data_bp = Blueprint('training_data_bp', __name__)


@training_data_bp.route('/', methods=['GET'])
def index():
    # return jsonify({'message': 'Hello from training data api'})
    data_training = DataTraining.query.all()
    data_training = [{
        'tag': data.tag,
        'patterns': data.patterns,
        'responses': data.responses
    } for data in data_training]
    return jsonify({'message': 'success', 'data': data_training})


@training_data_bp.route('/', methods=['POST'])
def create():
    body_request = request.json
    tag = body_request['tag']
    patterns = body_request['patterns']
    responses = body_request['responses']

    try:
        data_training = DataTraining(tag=tag, patterns=patterns, responses=responses)
        db.session.add(data_training)
        db.session.commit()
        return jsonify({'message': 'success', 'data': {
            'id': data_training.id,
            'tag': data_training.tag,
            'patterns': data_training.patterns,
            'responses': data_training.responses
        }}), 201

    except Exception as e:
        return jsonify({'message': str(e)}), 500
