import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Embedding, GlobalAveragePooling1D
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import nltk
from nltk.stem import WordNetLemmatizer
import json
import numpy as np
from flask import Blueprint, jsonify, request

# Download data NLTK untuk lemmatization
nltk.download('wordnet')

# Data training untuk chatbot
# training_data = [
#     {"tag": "greeting", "patterns": ["Hi", "Hello", "Hey"], "responses": ["Hello!", "Hi there!"]},
#     {"tag": "goodbye", "patterns": ["Bye", "Goodbye", "See you"], "responses": ["Goodbye!", "See you later!"]},
#     {"tag": "age", "patterns": ["How old are you?", "What's your age?"],
#      "responses": ["I am a computer program, ageless!"]},
#     {"tag": "lupa_no_kpj",
#      "patterns": ["Saya lupa nomor KPJ saya", "Saya lupa nomor KPJ", "Bagaimana cara mengetahui nomor KPJ"],
#      "responses": ["Untuk mengetahui nomor KPJ Anda, silahkan hubungi call center BPJS Kesehatan di nomor 1500400"]},
#     {"tag": "klaim_jht", "patterns": ["Bagaimana cara klaim JHT", "Bagaimana cara klaim Jaminan Hari Tua", "Cara claim JHT"], "responses": ["Untuk mengetahui cara klaim JHT, silahkan kunjungi https://www.bpjsketenagakerjaan.go.id/id/klaim-jht"]},
#     # Add more intents as needed
# ]

# Membaca data training dari file json
with open('training_data.json') as json_data:
    training_data = json.load(json_data)


# Membuat daftar kata unik dari training data
words = []
classes = []
documents = []
ignore_letters = ['!', '?', ',', '.']

for pattern in training_data:
    for pattern_text in pattern['patterns']:
        # Tokenisasi kata
        word_list = nltk.word_tokenize(pattern_text)
        words.extend(word_list)
        # Menyimpan pasangan kata dan intent
        documents.append((word_list, pattern['tag']))
        # Menambahkan ke daftar intent
        if pattern['tag'] not in classes:
            classes.append(pattern['tag'])

# Lemmatization dan konversi ke huruf kecil
lemmatizer = WordNetLemmatizer()
words = [lemmatizer.lemmatize(word.lower()) for word in words if word not in ignore_letters]
words = sorted(list(set(words)))
classes = sorted(list(set(classes)))

# Membuat training data
training = []
output_empty = [0] * len(classes)

for doc in documents:
    bag = []
    pattern_words = doc[0]
    pattern_words = [lemmatizer.lemmatize(word.lower()) for word in pattern_words]
    for word in words:
        bag.append(1) if word in pattern_words else bag.append(0)

    output_row = list(output_empty)
    output_row[classes.index(doc[1])] = 1
    training.append([np.array(bag), np.array(output_row)])

training = np.array(training, dtype=object)
training = np.array(training)

train_x = list(training[:, 0])
train_y = list(training[:, 1])

# Membuat model menggunakan TensorFlow
model = Sequential()
model.add(Dense(128, input_shape=(len(train_x[0]),), activation='relu'))
model.add(Dense(64, activation='relu'))
model.add(Dense(len(train_y[0]), activation='softmax'))

# Kompilasi model
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Melatih model
model.fit(np.array(train_x), np.array(train_y), epochs=100, batch_size=8, verbose=1)


# Fungsi untuk membentuk input pengguna dan mendapatkan output dari model
def clean_up_sentence(sentence):
    sentence_words = nltk.word_tokenize(sentence)
    sentence_words = [lemmatizer.lemmatize(word.lower()) for word in sentence_words]
    return sentence_words


def bow(sentence, words, show_details=True):
    sentence_words = clean_up_sentence(sentence)
    bag = [0] * len(words)
    for s in sentence_words:
        for i, w in enumerate(words):
            if w == s:
                bag[i] = 1
                if show_details:
                    print(f"found in bag: '{w}'")
    return (np.array(bag))


def predict_class(sentence, model):
    p = bow(sentence, words, show_details=False)
    res = model.predict(np.array([p]))[0]
    ERROR_THRESHOLD = 0.25
    results = [[i, r] for i, r in enumerate(res) if r > ERROR_THRESHOLD]
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
    return return_list


# Fungsi untuk mendapatkan respons dari chatbot
def get_response(intents_list, intents_json):
    tag = intents_list[0]['intent']
    list_of_intents = intents_json['intents']
    for i in list_of_intents:
        if i.get('tag') == tag:  # Check for 'tag' key
            result = np.random.choice(i['responses'])
            break
    return result


# Main loop chatbot
# print("Chatbot is ready. Type 'exit' to end the conversation.")
# while True:
#     user_input = input("You: ")
#     if user_input.lower() == 'exit':
#         break
#     else:
#         intents = predict_class(user_input, model)
#         response = get_response(intents, {'intents': training_data})
#         print("Bot:", response)


# Main loop chatbot
chat_bot_bp = Blueprint('chat_bot_bp', __name__)


@chat_bot_bp.route('/chat', methods=['POST'])
def chat_bot():
    # Mengambil input dari pengguna
    try:
        user_input = request.json['message']
        # Melakukan prediksi
        intents = predict_class(user_input, model)
        # Mendapatkan respons
        response = get_response(intents, {'intents': training_data})
        return jsonify({'message': 'success', 'response': response})
    except KeyError:
        return jsonify({'message': 'Bad request'}), 400


@chat_bot_bp.route('/', methods=['GET'])
def index():
    return jsonify({'message': 'Hello from chat bot api'})
